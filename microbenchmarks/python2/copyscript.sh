#!/bin/sh 

# this scrpit is used to copy the benchmarks in the differents impelentations 
# since we can't include them directly ( docker dosn't allow that)
# and sometimes we have to do some modefications inorder to be able of integration the optimisation tool ( adding jit decorators)

#s -R ../implementations | egrep  '[^/]*/[^/]*/[^/]*/[^/]*:' |sed  "s/\://g" |xargs -I {}  mkdir -p  "{}/benchmarks/$1" 
ls -R ../../implementations | egrep  -E '([^/]+/){5}[^/]*:' |sed  "s/\://g" |xargs -I {}  cp -R $1 "{}/benchmarks/" 
