class LoException(Exception):
    pass
    
class HiException(Exception):
    pass

class CustomExceptionMethod():
    def SomeFunction(self, n):
        try:
            self.HiFunction(n)
        except:
            print("We shouldn't get there")

    def HiFunction(self, n):
        try:
            self.LoFunction(n)
        except HiException:
            self.Hi = self.Hi + 1

    def LoFunction(self, n):
        try:
            self.Blowup(n)
        except LoException:
            self.Lo = self.Lo + 1

    def Blowup(self, n):
        if ((n & 1) == 0):
            raise LoException()
        else:
            raise HiException()
        
    def exceptF(self, n):
        self.Lo = 0
        self.Hi = 0

        while (n!=0):
            self.SomeFunction(n)
            n = n - 1

    def test(self):
        self.exceptF(10000)
        return True
 		
