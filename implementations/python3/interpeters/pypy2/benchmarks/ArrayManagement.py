from array import array

import os, sys
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(os.__file__))))))
os.sys.path.insert(0,parentdir) 
from programParameters import obtainParameterValue

class ArrayManagement:
    def arrayBench(self, n):
        i = 0
        j = 0
        k = 0
        if (n<1):
            n = 1
        x = array('i')
        y = array('i')
        for i in range(n):
            x.append(i+1)
            y.append(0)
        for k in range(1000):
            j = n - 1
            while(j>=0):
                y[j] += x[j]
                j = j - 1
            k = k + 1
            
    def test(self):
        self.arrayBench(obtainParameterValue("ArrayManagement", "n"))
        return True
 		
