import time
from cmath import *
from array import *
import sys
import math

class LoException(Exception):
    pass
    
class HiException(Exception):
    pass
    


class TommtiBenchmark:
    startTime = 0
    stopTime = 0
    elapsedTime = 0
    Lo = 0
    Hi = 0
    IM = 139968.0
    IA = 3877
    IC = 29573
    last = 42
    lSIZE = 100
    mSIZE = 30
    

    def intArithmetic (self, intMax):
        intResult = 1
        i = 0
        while (i<intMax):
            intResult -= i
            i = i + 1
            intResult += i
            i = i + 1
            intResult *= i
            i = i + 1 
            intResult /= i
            i = i + 1

        return True
 		

    def doubleArithmetic(self, doubleMin, doubleMax):
        doubleResult = doubleMin
        i = doubleMin
        while (i<doubleMax):
            doubleResult -= i
            i = i + 1
            doubleResult += i
            i = i + 1
            doubleResult *= i
            i = i + 1
            doubleResult /= i
            i = i + 1

        return True
 		

    def longArithmetic(self, intMin, intMax):
        intResult = intMin
        i = intMin
        while (i<intMax):
            intResult -= i
            i = i + 1
            intResult += i
            i = i + 1
            intResult *= i
            i = i + 1
            intResult /= i
            i = i + 1

        return True
 		

    def trig(self, trigMax):
        sine = 0.0
        cosine = 0.0
        tangent = 0.0
        logarithm = 0.0
        squareRoot = 0.0
        i = 1
        while (i<trigMax):
            sine = sin(i)
            cosine = cos(i)
            tangent = tan(i)
            logarithm = log(i, 10)
            squareRoot = sqrt(i)
            i = i + 1

        return True
 		

    def io(self, ioMax):
        filename = "\\TestCSharp.txt"
        textLine = "abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefgh\n"
        i = 0
        myLine = ""
        try:
            streamWriter = file(filename, 'w')
            while (i<=ioMax):
                i = i + 1
                streamWriter.write(textLine)
            streamWriter.close()
            i = 0
            # print("Reading file...")
            streamReader = file(filename, 'r')
            while(i<=ioMax):
                i = i + 1
                myLine = streamReader.readline()
        except:
            print("IOException")

        return True
 		

    def array(self, n):
        i = 0
        j = 0
        k = 0
        if (n<1):
            n = 1
        x = array('i')
        y = array('i')
        for i in range(n):
            x.append(i+1)
            y.append(0)
        for k in range(1000):
            j = n - 1
            while(j>=0):
                y[j] += x[j]
                j = j - 1
            k = k + 1

        return True
 		

    def SomeFunction(self, n):
        try:
            self.HiFunction(n)
        except:
            print("We shouldn't get there")

    def HiFunction(self, n):
        try:
            self.LoFunction(n)
        except HiException:
            self.Hi = self.Hi + 1

    def LoFunction(self, n):
        try:
            self.Blowup(n)
        except LoException:
            self.Lo = self.Lo + 1

    def Blowup(self, n):
        if ((n & 1) == 0):
            raise LoException()
        else:
            raise HiException()
        
    def exceptF(self, n):
        self.Lo = 0
        self.Hi = 0

        while (n!=0):
            self.SomeFunction(n)
            n = n - 1
        
        return True

    def hashtest(self, n):
        #X = dict()
        X = {}
        c = 0
        strTemp = "0x"
        if (n<1):
            n = 1
        i = 1
        while(i<n):
            try:
                X[str(hex(i))] = i
                i = i + 1
            except:
                pass
        i = n
        while(i>0):
            try:
                temp = X[str(strTemp) + str(i)]
                c = c + 1
            except:
                pass
            i = i - 1
  
        return True
 		

    def hashes(self, n):
        #hash1 = dict()
        #hash2 = dict()
        hash1 = {}
        hash2 = {}
        i = 0
        j = 0
        for i in range(1000):
            key = "foo_" + str(i)
            hash1[key] = i
        i = 0
        for i in range(n):
            for j in range(1000):
                hash2["foo_" + str(j)] = hash1["foo_" + str(j)] = j

        key = "foo_" + str(n-1)

        return True
 		

    def gen_random(self, max):
        self.last = (self.last * self.IA + self.IC) % self.IM 
        return max * self.last / self.IM
        
    def heapsort2(self, ra, Length):
        l=0
        j=0
        ir=0
        i=0
        rra=0.0
        l = ((Length-1) >> 1) + 1
        ir = (Length-1)
        while (1):
            if (l > 1):
                l = l - 1
                rra = ra[l]
            else:
                rra = ra[ir]
                ra[ir] = ra[1]
                ir = ir - 1
                if (ir == 1):
                    ra[1] = rra
                    return				
            i = l
            j = l << 1
            while (j <= ir):
                if ((j < ir) and (ra[j] < ra[j+1])):
                    j = j + 1
                if (rra < ra[j]):
                    ra[i] = ra[j]
                    i = j
                    j += i
                else:
                    j = ir + 1
            ra[i] = rra

    def heapsort(self, count):
        ary = array('d')
        i = 0
        for i in range(count):
            ary.append(self.gen_random(1))
       
        self.heapsort2(ary, count)

        return True
 		

    def VectorTest(self):
        Li1 = []#array('i')
        i = 1
        Count = self.lSIZE + 1
        for i in range(self.lSIZE + 1):
            Li1.append(i)
        Li2 = []#array('i')#Li1
        i = 1
        for i in range(self.lSIZE + 1):
            Li2.append(Li1[i])
            
        Li3 = []#array('i')
        tCount = Count
        while (tCount > 0):
            Li3.append(Li2[0])
            Li2.pop(0)
            tCount = tCount - 1
        tCount = Count
        while (tCount > 0):
            Li2.append(Li3[tCount-1])
            Li3.pop()
            tCount = tCount - 1
        tmp = []#array('i')
        tCount = Count
        while (tCount > 0):
            tmp.insert(0, Li1[0])
            Li1.pop(0)
            tCount = tCount - 1
        Li1 = tmp
        if (Li1[0] != self.lSIZE):
            # print("first item of Li1 != lSIZE")
            return 0
        i = 0
        for i in range(Count):
            if (Li1[i] != Li2[i]):
                # print("Li1 and Li2 differ")
                return 0
        return Count
    
    def vector(self, n):
        result = 0
        i = 0
        for i in range(n):
            result = self.VectorTest()

        return True
 		

    def mkmatrix(self, rows, cols):
        count = 1
        m = [None]*rows
        i = 0
        j = 0
        for i in range(rows):
            m[i] = [None] * cols
            for j in range(cols):
                count = count + 1
                m[i][j] = count
        return m

    def mmult(self, rows, cols, m1, m2, m3):
        i = 0
        j = 0
        k = 0
        for i in range(rows):
            for j in range(cols):
                val = 0
                for k in range(cols):
                    val += m1[i][k] * m2[k][j]
                m3[i][j] = val

    def matrixMultiply(self, n):
        m1 = self.mkmatrix(self.mSIZE, self.mSIZE)
        m2 = self.mkmatrix(self.mSIZE, self.mSIZE)
        mm = self.mkmatrix(self.mSIZE, self.mSIZE)
        i = 0
        if (n<1):
            n = 1
        for i in range(n):
            self.mmult(self.mSIZE, self.mSIZE, m1, m2, mm)

        return True
 		

    def nestedLoop(self, n):
        a = 0
        b = 0
        c = 0
        d = 0
        e = 0
        f = 0
        x = 0
        if (n<1):
            n = 1
        for a in range(n):
            for b in range(n):
                for c in range(n):
                    for d in range(n):
                        for e in range(n):
                            for f in range(n):
                                x=a+b+c+d+e+f

        return True
 		

    def stringConcat(self, N):
        if (N<1):
            N = 1
        sb = b"        "
        for i in range(N):
            sb = sb + b"."

        return True
 		
    

import os, sys
parentdir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(os.__file__))))))
os.sys.path.insert(0,parentdir) 
from programParameters import obtainParameterValue


intMax = obtainParameterValue("tommtiV2", "intMax")
doubleMin = float(obtainParameterValue("tommtiV2", "doubleMin"))
doubleMax = float(obtainParameterValue("tommtiV2", "doubleMax"))
longMin = long(obtainParameterValue("tommtiV2", "longMin"))
longMax = long(obtainParameterValue("tommtiV2", "longMax"))
trigMax = float(obtainParameterValue("tommtiV2", "trigMax"))
ioMax = obtainParameterValue("tommtiV2", "ioMax")

n  = obtainParameterValue("tommtiV2", "n")
    
def __intArithmetic():
    return TommtiBenchmark().intArithmetic(intMax)

def __doubleArithmetic():
    return TommtiBenchmark().doubleArithmetic(doubleMin, doubleMax)

def __longArithmetic():
    return TommtiBenchmark().longArithmetic(longMin, longMax)

def __trig():
    return TommtiBenchmark().trig(trigMax)

def __array():
    return TommtiBenchmark().array(n*1000)

def __hashtest():
    return TommtiBenchmark().hashtest(n*100000)

def __hashes():    
    return TommtiBenchmark().hashes(n*100)

def __heapsort():
    return TommtiBenchmark().heapsort(n*100000)

def __vector():
    return TommtiBenchmark().vector(n*10000)

def __matrixMultiply():
    return TommtiBenchmark().matrixMultiply(n*1000)

def __nestedLoop():
    return TommtiBenchmark().nestedLoop(n*2)

def __stringConcat():
    return TommtiBenchmark().stringConcat(n*10000)
    
def __io():
    return TommtiBenchmark().io(n*ioMax)

def __except():
    return TommtiBenchmark().exceptF(n*100000)
    

        

