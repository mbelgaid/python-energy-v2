import time
from cmath import *
from array import *
import sys
import math

class LoException(Exception):
    pass
    
class HiException(Exception):
    pass
    


class Benchmark_CSharp:
    startTime = 0
    stopTime = 0
    elapsedTime = 0
    Lo = 0
    Hi = 0
    IM = 139968.0
    IA = 3877
    IC = 29573
    last = 42
    lSIZE = 100
    mSIZE = 30
    

    def intArithmetic (self, intMax):
        elapsedMiliseconds = 0
        start_time = time.time()
        intResult = 1
        i = 0
        while (i<intMax):
            intResult -= i
            i = i + 1
            intResult += i
            i = i + 1
            intResult *= i
            i = i + 1 
            intResult /= i
            i = i + 1

        stop_time = time.time()
        # print("Int Arithmetic elapsed time", elapsedMiliseconds, " sec with max of ", intMax)
        # print("i: ", i)
        # print(" intResult", intResult)
##        return (stop_time-start_time)*1000
        return True
 		

    def doubleArithmetic(self, doubleMin, doubleMax):
        elapsedMiliseconds = 0
        start_time = time.time()
        doubleResult = doubleMin
        i = doubleMin
        while (i<doubleMax):
            doubleResult -= i
            i = i + 1
            doubleResult += i
            i = i + 1
            doubleResult *= i
            i = i + 1
            doubleResult /= i
            i = i + 1
        stop_time = time.time()
        # print("Double arithmetic elapsed time: ", elapsedMiliseconds, " sec with min of ", doubleMin, ", max of ", doubleMax)
        # print(" i: ", i)
        # print(" doubleResult: ", doubleResult)
##        return (stop_time-start_time)*1000
        return True
 		

    def longArithmetic(self, intMin, intMax):
        elapsedMiliseconds = 0
        start_time = time.time()
        intResult = intMin
        i = intMin
        while (i<intMax):
            intResult -= i
            i = i + 1
            intResult += i
            i = i + 1
            intResult *= i
            i = i + 1
            intResult /= i
            i = i + 1
        stop_time = time.time()
        # print("Long arithmetic elapsed time: ", elapsedMiliseconds, " sec with min of ", intMin, ", max of ", intMax)
        # print(" i: ", i)
        # print(" longResult: ", intResult)
##        return (stop_time-start_time)*1000
        return True
 		

    def trig(self, trigMax):
        elapsedMiliseconds = 0
        start_time = time.time()
        sine = 0.0
        cosine = 0.0
        tangent = 0.0
        logarithm = 0.0
        squareRoot = 0.0
        i = 0.0
        while (i<trigMax):
            sine = sin(i)
            cosine = cos(i)
            tangent = tan(i)
            logarithm = log(i, 10)
            squareRoot = sqrt(i)
            i = i + 1
        stop_time = time.time()
        # print("Trig elapsed time: ", elapsedMiliseconds, " sec with max of ", trigMax)
        # print(" i: ", i)
        # print(" sine: ", sine)
        # print(" cosine: ", cosine)
        # print(" tangent: ", tangent)
        # print(" logarithm: ", logarithm)
        # print(" squareRoot: ", squareRoot)
##        return (stop_time-start_time)*1000;
        return True
 		

    def io(self, ioMax):
        elapsedMiliseconds = 0
        start_time = time.time()
        filename = "C:\\TestCSharp.txt"
        textLine = "abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefgh\n"
        i = 0
        myLine = ""
        try:
            streamWriter = file(filename, 'w')
            while (i<=ioMax):
                i = i + 1
                streamWriter.write(textLine)
            streamWriter.close()
            i = 0
            # print("Reading file...")
            streamReader = file(filename, 'r')
            while(i<=ioMax):
                i = i + 1
                myLine = streamReader.readline()
        except:
            print("IOException")
        stop_time = time.time()
        # print("IO elapsed time: ", elapsedMiliseconds, " sec with max of ", ioMax)
        # print(" i: ", i)
        # print(" my Line: ", myLine)
##        return (stop_time-start_time)*1000
        return True
 		

    def array(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        i = 0
        j = 0
        k = 0
        if (n<1):
            n = 1
        x = array('i')
        y = array('i')
        for i in range(n):
            x.append(i+1)
            y.append(0)
        for k in range(1000):
            j = n - 1
            while(j>=0):
                y[j] += x[j]
                j = j - 1
            k = k + 1
        stop_time = time.time()
        # print("Array elapsed time: ", elapsedMiliseconds, " sec - ", y[0], " ", y[n-1])
##        return (stop_time-start_time)*1000
        return True
 		

    def SomeFunction(self, n):
        try:
            self.HiFunction(n)
        except:
            print("We shouldn't get there")

    def HiFunction(self, n):
        try:
            self.LoFunction(n)
        except HiException:
            self.Hi = self.Hi + 1

    def LoFunction(self, n):
        try:
            self.Blowup(n)
        except LoException:
            self.Lo = self.Lo + 1

    def Blowup(self, n):
        if ((n & 1) == 0):
            raise LoException()
        else:
            raise HiException()
        
    def exceptF(self, n):
        self.Lo = 0
        self.Hi = 0
        elapsedMiliseconds = 0
        start_time = time.time()
        while (n!=0):
            self.SomeFunction(n)
            n = n - 1
        stop_time = time.time()
        # print("Exception elapsed time: ", elapsedMiliseconds, " sec - Exceptions: HI=", self.Hi, " LO=", self.Lo)
        return (stop_time-start_time)*1000

    def hashtest(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        #X = dict()
        X = {}
        c = 0
        strTemp = "0x"
        if (n<1):
            n = 1
        i = 1
        while(i<n):
            try:
                X[str(hex(i))] = i
                i = i + 1
            except:
                pass
        i = n
        while(i>0):
            try:
                temp = X[str(strTemp) + str(i)]
                c = c + 1
            except:
                pass
            i = i - 1
        stop_time = time.time()
        # print("HashMap elapsed time: ", elapsedMiliseconds, " sec - ", c)
##        return (stop_time-start_time)*1000    
        return True
 		

    def hashes(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        #hash1 = dict()
        #hash2 = dict()
        hash1 = {}
        hash2 = {}
        i = 0
        j = 0
        for i in range(1000):
            key = "foo_" + str(i)
            hash1[key] = i
        i = 0
        for i in range(n):
            for j in range(1000):
                hash2["foo_" + str(j)] = hash1["foo_" + str(j)] = j
        stop_time = time.time()
        key = "foo_" + str(n-1)
        # print("HashMaps elapsed time: ", elapsedMiliseconds, " sec - ", str(hash1["foo_1"]) + " " + str(hash1[key]) + " " + str(hash2["foo_1"]) + " " + str(hash2[key]))
##        return (stop_time-start_time)*1000
        return True
 		

    def gen_random(self, max):
        self.last = (self.last * self.IA + self.IC) % self.IM 
        return max * self.last / self.IM
        
    def heapsort2(self, ra, Length):
        l=0
        j=0
        ir=0
        i=0
        rra=0.0
        l = ((Length-1) >> 1) + 1
        ir = (Length-1)
        while (1):
            if (l > 1):
                l = l - 1
                rra = ra[l]
            else:
                rra = ra[ir]
                ra[ir] = ra[1]
                ir = ir - 1
                if (ir == 1):
                    ra[1] = rra
                    return				
            i = l
            j = l << 1
            while (j <= ir):
                if ((j < ir) and (ra[j] < ra[j+1])):
                    j = j + 1
                if (rra < ra[j]):
                    ra[i] = ra[j]
                    i = j
                    j += i
                else:
                    j = ir + 1
            ra[i] = rra

    def heapsort(self, count):
        elapsedMiliseconds = 0
        start_time = time.time()
        ary = array('d')
        i = 0
        for i in range(count):
            ary.append(self.gen_random(1))
       
        self.heapsort2(ary, count)
        stop_time = time.time()
        # print "HeapSort elapsed time: ", elapsedMiliseconds, " sec - ", ary[count-1]
##        return (stop_time - start_time)*1000
        return True
 		

    def VectorTest(self):
        Li1 = []#array('i')
        i = 1
        Count = self.lSIZE + 1
        for i in range(self.lSIZE + 1):
            Li1.append(i)
        Li2 = []#array('i')#Li1
        i = 1
        for i in range(self.lSIZE + 1):
            Li2.append(Li1[i])
            
        Li3 = []#array('i')
        tCount = Count
        while (tCount > 0):
            Li3.append(Li2[0])
            Li2.pop(0)
            tCount = tCount - 1
        tCount = Count
        while (tCount > 0):
            Li2.append(Li3[tCount-1])
            Li3.pop()
            tCount = tCount - 1
        tmp = []#array('i')
        tCount = Count
        while (tCount > 0):
            tmp.insert(0, Li1[0])
            Li1.pop(0)
            tCount = tCount - 1
        Li1 = tmp
        if (Li1[0] != self.lSIZE):
            # print("first item of Li1 != lSIZE")
            return 0
        i = 0
        for i in range(Count):
            if (Li1[i] != Li2[i]):
                # print("Li1 and Li2 differ")
                return 0
        return Count
    
    def vector(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        result = 0
        i = 0
        for i in range(n):
            result = self.VectorTest()
        stop_time = time.time()
        # print("Vector elapsed time: ", elapsedMiliseconds, " sec - ", result)
##        return (stop_time-start_time)*1000
        return True
 		

    def mkmatrix(self, rows, cols):
        count = 1
        m = [None]*rows
        i = 0
        j = 0
        for i in range(rows):
            m[i] = [None] * cols
            for j in range(cols):
                count = count + 1
                m[i][j] = count
        return m

    def mmult(self, rows, cols, m1, m2, m3):
        i = 0
        j = 0
        k = 0
        for i in range(rows):
            for j in range(cols):
                val = 0
                for k in range(cols):
                    val += m1[i][k] * m2[k][j]
                m3[i][j] = val

    def matrixMultiply(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        m1 = self.mkmatrix(self.mSIZE, self.mSIZE)
        m2 = self.mkmatrix(self.mSIZE, self.mSIZE)
        mm = self.mkmatrix(self.mSIZE, self.mSIZE)
        i = 0
        if (n<1):
            n = 1
        for i in range(n):
            self.mmult(self.mSIZE, self.mSIZE, m1, m2, mm)
        stop_time = time.time()
        # print("Matrix multiply elapsed time: ", elapsedMiliseconds, " sec - ", str(mm[0][0]) + " " + str(mm[2][3]) + " " + str(mm[3][2]) + " " + str(mm[4][4]))
##        return (stop_time-start_time)*1000
        return True
 		

    def nestedLoop(self, n):
        elapsedMiliseconds = 0
        start_time = time.time()
        a = 0
        b = 0
        c = 0
        d = 0
        e = 0
        f = 0
        x = 0
        if (n<1):
            n = 1
        for a in range(n):
            for b in range(n):
                for c in range(n):
                    for d in range(n):
                        for e in range(n):
                            for f in range(n):
                                x+=a+b+c+d+e+f
        stop_time = time.time()
        # print("Nested loop elapsed time: ", elapsedMiliseconds, " sec - ", x)
##        return (stop_time-start_time)*1000
        return True
 		

    def stringConcat(self, N):
        elapsedMiliseconds = 0
        start_time = time.time()
        if (N<1):
            N = 1
        sb = b"        "
        for i in range(N):
            sb = sb + b"."
        stop_time = time.time()
        # print("String concat elapsed time: ", elapsedMiliseconds, " sec - ", len)
##        return (stop_time-start_time)*1000
        return True
 		
    

if __name__ == "__main__":

    intMax = 100000000
    doubleMin = 1000000000.0
    doubleMax = 1100000000.0 
    longMin = 1000000000
    longMax = 1100000000
    trigMax = 1000000.0
    ioMax = 10000

    n  = 10
        
    def __intArithmetic(obj):
        return obj.intArithmetic(intMax)

    def __doubleArithmetic(obj):
        return obj.doubleArithmetic(doubleMin, doubleMax)

    def __longArithmetic(obj):
        return obj.longArithmetic(longMin, longMax)

    def __trig(obj):
        return obj.trig(trigMax)

    def __array(obj):
        return obj.array(n*1000)

    def __hashtest(obj):
        return obj.hashtest(n*1000)

    def __hashes(obj):    
        return obj.hashes(n*10)

    def __heapsort(obj):
        return obj.heapsort(n*10000)

    def __vector(obj):
        return obj.vector(n*10)

    def __matrixMultiply(obj):
        return obj.matrixMultiply(n*1000)

    def __nestedLoop(obj):
        return obj.nestedLoop(n*2)

    def __stringConcat(obj):
        return obj.stringConcat(n*10000)
    
    def areWeDone(executionTimes, k, CoV):
            if (len(executionTimes)<k):
                    return False
            summation = 0
            mean = getMean(executionTimes, k)
            for i in range (len(executionTimes)-k, len(executionTimes)):
                    summation += math.pow(executionTimes[i]-mean, 2)
            stdDeviation = math.sqrt(summation / k)
            return stdDeviation / mean < CoV
            
            
    def getMean(executionTimes, k):
        summation = 0
        for i in range (k):
            summation = summation + executionTimes[len(executionTimes)-i-1]
        return summation / k
	
    if len(sys.argv) == 1:
        print("Pass me the name of the test to be run")
        sys.exit(-1)

    obj = Benchmark_CSharp()
    
    if len(sys.argv) == 2:
        # Startup
        execution_time = eval("__"+sys.argv[1]+"(obj)")*1000
        sys.exit(int(execution_time))
        
    if (len(sys.argv)<5): 
        print("I need 1) the number of the test 2) the max number of iterations 3) the k 4) the CoV.")
        sys.exit(-1)
	
    #Steady
    maxNumberIterations = int(sys.argv[2])
    k = int(sys.argv[3])
    CoV = float(sys.argv[4])
		
    execution_times = []
    for i in range(1, maxNumberIterations+1):
        execution_time = eval("__"+sys.argv[1]+"(obj)")*1000
        execution_times.append(execution_time)
        if (areWeDone(execution_times, k, CoV)):
            break
		
    sys.exit(int(getMean(execution_times, k)))

    

        

